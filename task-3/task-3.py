import numpy as np

with open('input', 'r') as file:
    file_content = file.readlines()
    file_content = list(map(str.strip, file_content))

file_content = list(map(list, file_content))

int_content = np.asarray(file_content).astype(int)
num_rows = len(int_content)
summ = np.sum(int_content, 0)

gamma_rate = summ > num_rows / 2
epsilon_rate = np.logical_not(gamma_rate)