import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import dijkstra

with open('input', 'r') as file:
    file_content = file.readlines()
    file_content = list(map(str.strip, file_content))

width = len(file_content)
height = len(file_content[0])

neighbours = np.zeros((width * height, width * height))

for i, line in enumerate(file_content):
    for j, letter in enumerate(line):
        # c[i][j] = i -> j

        if j > 0:
            neighbours[i * width + j - 1][i * width + j] = int(letter)

        if j < width - 1:
            neighbours[i * width + j + 1][i * width + j] = int(letter)

        if i > 0:
            neighbours[(i-1) * width + j][i * width + j] = int(letter)

        if i < height - 1:
            neighbours[(i+1) * width + j][i * width + j] = int(letter)


graph = csr_matrix(neighbours)

if __name__ == '__main__':
    assert dijkstra(graph,indices=0, min_only=True)[-1] == 581