with open('input', 'r') as file:
    file_content = file.readlines()
    file_content = list(map(str.strip, file_content))

file_content = list(map(lambda x: x.split(' '), file_content))

forward_amount = 0
depth = 0

for command, amount in file_content:
    amount = int(amount)

    if command == 'forward':
        forward_amount += amount
    elif command == 'down':
        depth += amount
    elif command == 'up':
        depth -= amount
        depth = max(0, depth)

print(forward_amount * depth)

# part 2

forward_amount = 0
depth = 0
aim = 0

for command, amount in file_content:
    amount = int(amount)

    if command == 'forward':
        forward_amount += amount
        depth += amount * aim
    elif command == 'down':
        aim += amount
    elif command == 'up':
        aim -= amount

print(forward_amount * depth)