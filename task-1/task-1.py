with open('input', 'r') as file:
    file_content = file.readlines()
    file_content = list(map(str.strip, file_content))

numbers = list(map(int, file_content))

num_increases = 0
for number1, number2 in zip(numbers[:-1], numbers[1:]):
    if number2 > number1:
        num_increases += 1

# part 2
sums = [i + j + k for i,j,k in zip(numbers[:-2], numbers[1:-1], numbers[2:])]

num_increases_2 = 0
for number1, number2 in zip(sums[:-1], sums[1:]):
    if number2 > number1:
        num_increases_2 += 1

if __name__ == '__main__':
    assert num_increases == 1446
    assert num_increases_2 == 1486
